package kslesik.pl.tvroute.converters;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;

/**
 * Created by maxym on 07.02.2016.
 */
public class StringToJSONArrayConverter implements Converter {

    @Override
    public JSONArray fromBody(TypedInput body, Type type) throws ConversionException {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(body.in()));
            String status = bufferedReader.readLine();
            String jsonArrayString = bufferedReader.readLine();

            if(!status.equals("ok")) {
                Log.wtf(StringToJSONArrayConverter.class.getName(), "Method error: "+status);
                return new JSONArray();
            }

            return new JSONArray(jsonArrayString);
        } catch (JSONException | IOException e) {
            throw new ConversionException("Method error", e);
        }
    }

    @Override
    public TypedOutput toBody(Object object) {
        throw new UnsupportedOperationException();
    }

}
