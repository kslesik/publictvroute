package kslesik.pl.tvroute.converters;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import kslesik.pl.tvroute.model.Channel;
import retrofit.converter.ConversionException;
import retrofit.converter.JacksonConverter;
import retrofit.mime.TypedInput;

/**
 * Created by maxym on 05.02.2016.
 */
public class StringToChannelConverter extends JacksonConverter {

    private final Map<String, Channel> channelsList;
    private final ObjectMapper mapper;

   @Inject
   public StringToChannelConverter(Map<String, Channel> channelsList){
       this.channelsList = channelsList;
       this.mapper = new ObjectMapper();
   }

    @Override
    public List<Channel> fromBody(TypedInput body, Type type) throws ConversionException {
        JavaType channelsStringType = mapper.getTypeFactory().constructCollectionType(List.class, String.class);
        try {
            List<String> channelsNames = mapper.readValue(body.in(), channelsStringType);
            List<Channel> channelsList = new ArrayList<>(channelsNames.size());
            for(String channelName : channelsNames)
                channelsList.add(this.channelsList.get(channelName));
            return channelsList;
        } catch (IOException e) {
            throw new ConversionException(e);
        }
    }
}
