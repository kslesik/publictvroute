package kslesik.pl.tvroute.api.filmweb.calls.get;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

import kslesik.pl.tvroute.api.filmweb.calls.ApiMethodCall;
import kslesik.pl.tvroute.api.filmweb.helpers.MethodType;
import lombok.Setter;

/**
 * Created by maxym on 08.01.2016.
 */
public class GetUserVotes extends ApiMethodCall {

    private @Setter Long userID;

    private Map<Long, Long> filmsVoted = new HashMap<>();

    public boolean ifVoted(Long filmID){
        return filmsVoted.containsKey(filmID);
    }

    public long getFilmRating(long filmID){
        return filmsVoted.get(filmID);
    }

    @Override
    public void parse() {
        for (int i = 0; i < rawResponse.length(); ++i) {
            JSONArray filmVotedJSON = rawResponse.optJSONArray(i);
            if (filmVotedJSON != null)
                this.filmsVoted.put(filmVotedJSON.optLong(0), filmVotedJSON.optLong(2));
        }
    }

    @Override
    public Map<Long, Long> getResponse() {
        return filmsVoted;
    }

    @Override
    public String getMethodName() {
        return String.format("getUserFilmVotes [%s,0,200]", userID);
    }

    @Override
    public MethodType getMethodType() {
        return MethodType.GET;
    }
}
