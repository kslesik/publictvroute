package kslesik.pl.tvroute.api.filmweb.calls.get;

import org.json.JSONArray;

import java.util.HashSet;
import java.util.Set;

import kslesik.pl.tvroute.api.filmweb.calls.ApiMethodCall;
import kslesik.pl.tvroute.api.filmweb.helpers.MethodType;
import lombok.Setter;

/**
 * Created by maxym on 08.01.2016.
 */
public class GetUserFilmWantToSee extends ApiMethodCall {

    private @Setter Long userID;

    private Set<Long> filmsWantToSee = new HashSet<>();

    public boolean ifWantToSee(Long filmID){
        return filmsWantToSee.contains(filmID);
    }

    @Override
    public void parse() {
        for (int i = 0; i < rawResponse.length(); ++i) {
            JSONArray filmWantToSeeJSON = rawResponse.optJSONArray(i);
            if (filmWantToSeeJSON != null)
                this.filmsWantToSee.add(filmWantToSeeJSON.optLong(0));
        }
    }

    @Override
    public Set<Long> getResponse() {
        return filmsWantToSee;
    }

    @Override
    public String getMethodName() {
        return String.format("getUserFilmsWantToSee [%s,1.0]", userID);
    }

    @Override
    public MethodType getMethodType() {
        return MethodType.GET;
    }
}
