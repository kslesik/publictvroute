package kslesik.pl.tvroute.api.filmweb.helpers;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by maxym on 07.01.2016.
 */
public class ApiHelper {

    final private static char[] hexArray = "0123456789abcdef".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length << 1];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String calcSignature(String signature) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(signature.getBytes("UTF-8"));

            return bytesToHex(digest.digest());

        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            Log.wtf(ApiHelper.class.getName(), e);
        }

        return "";
    }

}
