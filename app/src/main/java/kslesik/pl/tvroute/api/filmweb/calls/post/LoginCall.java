package kslesik.pl.tvroute.api.filmweb.calls.post;

import android.content.res.Resources;

import javax.inject.Inject;

import kslesik.pl.tvroute.R;
import kslesik.pl.tvroute.api.filmweb.calls.ApiMethodCall;
import kslesik.pl.tvroute.api.filmweb.helpers.MethodType;
import kslesik.pl.tvroute.model.User;
import lombok.Getter;

/**
 * Created by maxym on 07.01.2016.
 */
public class LoginCall extends ApiMethodCall {

    private @Getter User user;
    private final String filmwebUserName;
    private final String filmwebPassword;

    @Inject
    public LoginCall(Resources resources){
        this.filmwebUserName = resources.getString(R.string.filmwebUserName);
        this.filmwebPassword = resources.getString(R.string.filmwebUserPass);
    }

    @Override
    public void parse() {
        user = rawResponse.length() == 0 ? null : new User(rawResponse);
    }

    @Override
    public User getResponse() {
        return user;
    }

    @Override
    public String getMethodName() {
        return String.format("login [\"%s\", \"%s\", 0]", filmwebUserName, filmwebPassword);
    }

    @Override
    public MethodType getMethodType() {
        return MethodType.POST;
    }
}
