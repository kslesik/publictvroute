package kslesik.pl.tvroute.api.filmweb.calls;

import org.json.JSONArray;

import java.util.LinkedHashMap;
import java.util.Map;

import kslesik.pl.tvroute.api.filmweb.helpers.ApiHelper;
import kslesik.pl.tvroute.api.filmweb.helpers.MethodType;
import lombok.Setter;

/**
 * Created by maxym on 07.01.2016.
 */
public abstract class ApiMethodCall {

    protected @Setter JSONArray rawResponse;

    public abstract Object getResponse();
    public abstract String getMethodName();
    public abstract MethodType getMethodType();
    public abstract void parse();

    public Map<String, String> getParams(){
        Map<String, String> params = new LinkedHashMap<>();
        params.put("methods", getMethodName()+"\\n");
        params.put("signature", getSignature());
        params.put("version", "1.0");
        params.put("appId", "android");

        return params;
    }

    private String getSignature(){
        String signaturePattern = getMethodName() + "\\n" + "android" + "qjcGhW2JnvGT9dfCt3uT_jozR3s";
        return "1.0," + ApiHelper.calcSignature(signaturePattern);
    }

}
