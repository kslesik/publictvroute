package kslesik.pl.tvroute.api.filmweb;

import org.json.JSONArray;

import java.util.Map;

import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.QueryMap;

/**
 * Created by maxym on 07.02.2016.
 */
public interface FilmwebAPI {

    @POST("/api")
    JSONArray post(@QueryMap Map<String, String> params);

    @GET("/api")
    JSONArray get(@QueryMap Map<String, String> params);

}
