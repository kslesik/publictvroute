package kslesik.pl.tvroute.api.filmweb.calls.get;

import android.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.json.JSONArray;

import java.util.LinkedHashSet;
import java.util.Set;

import kslesik.pl.tvroute.api.filmweb.calls.ApiMethodCall;
import kslesik.pl.tvroute.api.filmweb.helpers.MethodType;
import kslesik.pl.tvroute.model.Channel;
import kslesik.pl.tvroute.model.ProgramTV;
import lombok.Getter;

/**
 * Created by maxym on 11.01.2016.
 */
public class GetChannelSchedule extends ApiMethodCall {

    private @Getter Channel channel;
    private Set<ProgramTV> programsMap = new LinkedHashSet<>();

    public void setChannel(Channel channel){
        this.channel = channel;
    }

    public Pair<ProgramTV, ProgramTV> getActuallyPlaying(){
        ProgramTV current = null;
        ProgramTV next = null;
        DateTime now = DateTime.now();
        for(ProgramTV program : programsMap){
            next = program;
            if(program.getTime().isAfter(now))
                break;
            current = program;
        }
        return new Pair<>(current, next);
    }

    @Override
    public Set<ProgramTV> getResponse() {
        return programsMap;
    }

    @Override
    public String getMethodName() {
        DateTime day  = LocalTime.now().getHourOfDay() < 4 ? DateTime.now().minusDays(1) : DateTime.now();
        return String.format("getTvSchedule [%d,%s]", this.channel.getChannelID(), day.toString("yyyy-M-d"));
    }

    @Override
    public MethodType getMethodType() {
        return MethodType.GET;
    }

    @Override
    public void parse() {
        programsMap.clear();
        for (int i = 0; i < rawResponse.length(); ++i) {
            JSONArray programJSON = rawResponse.optJSONArray(i);
            if (programJSON != null)
                programsMap.add(new ProgramTV(programJSON));
        }

    }
}
