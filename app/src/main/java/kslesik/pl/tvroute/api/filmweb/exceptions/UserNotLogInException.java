package kslesik.pl.tvroute.api.filmweb.exceptions;

import lombok.NoArgsConstructor;

/**
 * Created by maxym on 15.02.2016.
 */
@NoArgsConstructor
public class UserNotLogInException extends Throwable {
    public UserNotLogInException(String message){
        super(message);
    }
}
