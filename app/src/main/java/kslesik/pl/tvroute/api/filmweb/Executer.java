package kslesik.pl.tvroute.api.filmweb;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import javax.inject.Inject;
import javax.inject.Singleton;

import kslesik.pl.tvroute.api.filmweb.calls.ApiMethodCall;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetAllChannels;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserFilmWantToSee;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserVotes;
import kslesik.pl.tvroute.api.filmweb.calls.post.LoginCall;
import kslesik.pl.tvroute.api.filmweb.exceptions.UserNotLogInException;
import kslesik.pl.tvroute.model.User;
import lombok.Getter;

@Singleton
public class Executer {

    final public static String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";

    private final LoginCall loginCall;
    private final GetUserFilmWantToSee getUserFilmWantToSee;
    private final GetUserVotes getUserVotes;
    private final GetAllChannels getAllChannels;
    private final FilmwebAPI filmwebAPI;

    private @Getter User user;

    @Inject
    public Executer(FilmwebAPI filmwebAPI,
                    LoginCall loginCall,
                    GetUserFilmWantToSee getUserFilmWantToSee,
                    GetUserVotes getUserVotes,
                    GetAllChannels getAllChannels){
        this.loginCall = loginCall;
        this.getUserFilmWantToSee = getUserFilmWantToSee;
        this.getUserVotes = getUserVotes;
        this.getAllChannels = getAllChannels;
        this.filmwebAPI = filmwebAPI;
    }

    public void getData() throws UserNotLogInException{
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        execute(loginCall);
        user = loginCall.getUser();
        if(user == null)
            throw new UserNotLogInException();
        getUserFilmWantToSee.setUserID(user.getUserID());
        getUserVotes.setUserID(user.getUserID());
        execute(getUserFilmWantToSee);
        execute(getUserVotes);
        execute(getAllChannels);
    }

    public void execute(ApiMethodCall call) {
        switch(call.getMethodType()){
            case GET:
                call.setRawResponse(filmwebAPI.get(call.getParams()));
                break;
            case POST:
                call.setRawResponse(filmwebAPI.post(call.getParams()));
                break;
        }

        call.parse();
    }

}
