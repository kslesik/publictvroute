package kslesik.pl.tvroute.api.filmweb.calls.get;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

import kslesik.pl.tvroute.api.filmweb.calls.ApiMethodCall;
import kslesik.pl.tvroute.api.filmweb.helpers.MethodType;
import kslesik.pl.tvroute.model.Channel;

/**
 * Created by maxym on 11.01.2016.
 */
public class GetAllChannels extends ApiMethodCall {

    private Map<String, Channel> channelsListMap = new HashMap<>();

    public Channel findByName(String channelName){
        return channelsListMap.get(channelName);
    }

    @Override
    public Map<String, Channel> getResponse() {
        return channelsListMap;
    }

    @Override
    public String getMethodName() {
        return "getAllChannels [1.0]";
    }

    @Override
    public MethodType getMethodType() {
        return MethodType.GET;
    }

    @Override
    public void parse() {
        for (int i = 0; i < rawResponse.length(); ++i) {
            JSONArray channelJSON = rawResponse.optJSONArray(i);
            if (channelJSON != null)
                channelsListMap.put(channelJSON.optString(1), new Channel(channelJSON));
        }
    }
}
