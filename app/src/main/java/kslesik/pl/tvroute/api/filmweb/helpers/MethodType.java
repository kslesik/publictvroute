package kslesik.pl.tvroute.api.filmweb.helpers;

/**
 * Created by maxym on 07.01.2016.
 */
public enum MethodType {
    GET("GET"),
    POST("POST");

    private String methodType;

    MethodType(String methodType){
        this.methodType = methodType;
    }
}
