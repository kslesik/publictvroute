package kslesik.pl.tvroute.api.router;

import java.util.List;

import kslesik.pl.tvroute.model.Channel;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by maxym on 03.02.2016.
 */

public interface RouterAPI {

    @GET("/start/{mux}")
    List<Channel> openMux(@Path("mux") int mux);

    @GET("/start")
    List<Channel> startStreaming();

    @GET("/stop")
    Response stopStreaming();
}
