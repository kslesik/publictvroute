package kslesik.pl.tvroute.api.filmweb.calls.get;

import kslesik.pl.tvroute.api.filmweb.calls.ApiMethodCall;
import kslesik.pl.tvroute.api.filmweb.helpers.MethodType;
import lombok.Setter;

/**
 * Created by maxym on 16.01.2016.
 */
public class GetProgramDescription extends ApiMethodCall {

    private @Setter long programID;
    private String programInfo;

    @Override
    public String getResponse() {
        return programInfo;
    }

    @Override
    public String getMethodName() {
        return String.format("getFilmDescription [%d]", programID);
    }

    @Override
    public MethodType getMethodType() {
        return MethodType.GET;
    }

    @Override
    public void parse() {
        programInfo = rawResponse.optString(0);
    }
}
