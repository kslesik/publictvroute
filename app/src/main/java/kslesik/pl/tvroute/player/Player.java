package kslesik.pl.tvroute.player;

import android.content.Context;
import android.content.res.Resources;
import android.view.Surface;
import android.view.SurfaceView;

import org.json.JSONException;
import org.videolan.libvlc.IVideoPlayer;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.LibVlcException;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import kslesik.pl.tvroute.R;
import kslesik.pl.tvroute.api.router.RouterAPI;
import kslesik.pl.tvroute.model.Channel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by maxym on 02.02.2016.
 */
@Getter
@Setter
public class Player implements IVideoPlayer {

    private String channelURLStream;
    private Integer muxCount;

    private LibVLC mLibVLC;
    private Integer currentlyPlayingChannelIndex;
    private Integer currentlyPlayingMux;
    private Channel currentlyPlayingChannel;
    private List<Channel> channelsListOnMux;
    private RouterAPI routerAPI;

    @Inject
    public Player(Resources resources, RouterAPI routerAPI) {
        this.routerAPI = routerAPI;
        this.channelURLStream = resources.getString(R.string.channelURLStream);
        this.muxCount = resources.getInteger(R.integer.MUX_COUNT);

        mLibVLC = new LibVLC();
        mLibVLC.setAout(mLibVLC.AOUT_AUDIOTRACK);
        mLibVLC.setVout(mLibVLC.VOUT_ANDROID_SURFACE);
        mLibVLC.setTimeStretching(true);
        mLibVLC.setFrameSkip(true);
        mLibVLC.setChroma("RV16");
        mLibVLC.setNetworkCaching(3000);
        mLibVLC.setHardwareAcceleration(LibVLC.HW_ACCELERATION_AUTOMATIC);
    }

    public void pause() {
        mLibVLC.pause();
    }

    public void play() {
        mLibVLC.play();
    }

    public void stop() {
        mLibVLC.stop();
    }

    public void close() {
        mLibVLC.stop();
        new Thread(new Runnable() {
            @Override
            public void run() {
                routerAPI.stopStreaming();
            }

        }).start();
    }


    public void init(SurfaceView channelSurface, Context context) throws LibVlcException {
        mLibVLC.attachSurface(channelSurface.getHolder().getSurface(), this);
        mLibVLC.init(context);
    }

    public void startStreaming() {
        channelsListOnMux = routerAPI.startStreaming();
        currentlyPlayingChannelIndex = 0;
        currentlyPlayingChannel = channelsListOnMux.get(currentlyPlayingChannelIndex);
    }

    public void startPlaying() {
        currentlyPlayingChannelIndex = 0;
        currentlyPlayingMux = 0;
        currentlyPlayingChannel = channelsListOnMux.get(currentlyPlayingChannelIndex);
        mLibVLC.playMRL(channelURLStream + (currentlyPlayingChannelIndex + 1));
    }

    public void playChannel(Integer channelIndex) {
        mLibVLC.stop();
        currentlyPlayingChannelIndex = channelIndex;
        currentlyPlayingChannel = channelsListOnMux.get(currentlyPlayingChannelIndex);
        mLibVLC.playMRL(channelURLStream + (currentlyPlayingChannelIndex + 1));
    }

    public void playNextChannel() {
        currentlyPlayingChannelIndex++;
        currentlyPlayingChannel = channelsListOnMux.get(currentlyPlayingChannelIndex);
        mLibVLC.stop();
        mLibVLC.playMRL(channelURLStream + (currentlyPlayingChannelIndex + 1));
    }


    public void playPreviousChannel() {
        currentlyPlayingChannelIndex--;
        currentlyPlayingChannel = channelsListOnMux.get(currentlyPlayingChannelIndex);
        mLibVLC.stop();
        mLibVLC.playMRL(channelURLStream + (currentlyPlayingChannelIndex + 1));
    }


    public void switchMux(boolean mode) throws IOException, JSONException {
        currentlyPlayingMux = mode ? (currentlyPlayingMux + 1) % muxCount : (currentlyPlayingMux - 1) % muxCount;
        if (currentlyPlayingMux < 0)
            currentlyPlayingMux += muxCount;

        channelsListOnMux = routerAPI.openMux(currentlyPlayingMux);
        currentlyPlayingChannelIndex = mode ? 0 : channelsListOnMux.size() - 1;
        currentlyPlayingChannel = channelsListOnMux.get(currentlyPlayingChannelIndex);
        mLibVLC.stop();
        mLibVLC.playMRL(channelURLStream + (currentlyPlayingChannelIndex + 1));
    }

    public boolean previousChannelAvaivaible() {
        return currentlyPlayingChannelIndex != 0;
    }

    public boolean nextChannelAvaivaible() {
        return currentlyPlayingChannelIndex != channelsListOnMux.size() - 1;
    }

    @Override
    public void setSurfaceLayout(int width, int height, int visible_width, int visible_height, int sar_num, int sar_den) {
    }

    @Override
    public int configureSurface(Surface surface, int width, int height, int hal) {
        return -1;
    }

    @Override
    public void eventHardwareAccelerationError() {
        return;
    }

    public void setSurface(SurfaceView surface) {
        mLibVLC.setSurface(surface.getHolder().getSurface());
    }
}


