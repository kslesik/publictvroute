package kslesik.pl.tvroute.model;

import org.json.JSONArray;
import org.json.JSONException;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by maxym on 10.01.2016.
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"title"})
public class FilmWantToSee {

    private long id;
    private String title;
    private String originalTitle;
    private int yearOfProduction;
    private String posterAddress;

    public FilmWantToSee(JSONArray filmWantToSeeJSON) throws JSONException {
        id = filmWantToSeeJSON.getLong(0);
        title = filmWantToSeeJSON.getString(1);
        originalTitle = filmWantToSeeJSON.getString(2);
        yearOfProduction = filmWantToSeeJSON.getInt(3);
        posterAddress = filmWantToSeeJSON.getString(4);
    }
}
