package kslesik.pl.tvroute.model;

import org.json.JSONArray;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by maxym on 03.01.2016.
 */
@NoArgsConstructor
public @Data class Channel implements Serializable {

    private String channelName;
    private long channelID;
    private String channelIconURL;

    public static final String CHANNELS_ICONS_URL = "http://1.fwcdn.pl/channels";

    public Channel(JSONArray data) {
        this.channelID  = data.optLong(0);
        this.channelName = data.optString(1);
        this.channelIconURL = data.optString(2);

        if(this.channelIconURL != null)
            this.channelIconURL = this.channelIconURL.replace("0.png", "2.png");
    }

}

