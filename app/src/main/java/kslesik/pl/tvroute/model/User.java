package kslesik.pl.tvroute.model;

import org.json.JSONArray;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by maxym on 17.01.2016.
 */
@NoArgsConstructor
public @Data class User {
    private Long userID;
    private String username;

    public User(JSONArray data){
        userID = data.optLong(3);
        username = data.optString(0);
    }
}
