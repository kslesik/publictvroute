package kslesik.pl.tvroute.model;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalTime;
import org.json.JSONArray;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by maxym on 15.01.2016.
 */
public @Data class ProgramTV implements Serializable{

    public static final String POSTERS_URL = "http://1.fwcdn.pl/po";
    private static final long DAY_DIVIDE_TIME = 4;

    private final long programID;
    private final String programName;
    private final String programInfo;
    private final String programImage;
    private final Long filmwebProgramID;
    private final DateTime time;

    public ProgramTV(JSONArray data) {
        this.programID  = data.optLong(0);
        this.programName = data.optString(1);
        this.programInfo = data.optString(2);
        String[] timeString = data.optString(3).split(":");
        DateTime day  = LocalTime.now().getHourOfDay() < DAY_DIVIDE_TIME ? DateTime.now().minus(Days.ONE) : DateTime.now();

        int hour = Integer.parseInt(timeString[0]);
        int minute = Integer.parseInt(timeString[1]);
        if(hour < DAY_DIVIDE_TIME)
            day = day.plus(Days.ONE);

        day = day.withHourOfDay(hour);
        day = day.withMinuteOfHour(minute);

        this.time = day;
        this.filmwebProgramID = data.optString(5).equals("null") ? null : data.optLong(5);

        this.programImage = data.optString(8).replace(".1.", ".3.");
    }
}
