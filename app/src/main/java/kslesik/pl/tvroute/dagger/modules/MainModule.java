package kslesik.pl.tvroute.dagger.modules;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kslesik.pl.tvroute.view.OverlayFragment;
import kslesik.pl.tvroute.view.OverlayFragment_;

/**
 * Created by maxym on 04.02.2016.
 */
@Module
public class MainModule {

    Application mApplication;

    public MainModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    Resources provideResources(){
        return mApplication.getResources();
    }

    @Provides
    Context provideContext(){ return mApplication.getBaseContext(); }

    @Provides
    @Singleton
    ImageLoader provideImageLoader(){
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mApplication));
        return imageLoader;
    }

    @Provides
    OverlayFragment provideOverlayFragment(){
        return new OverlayFragment_();
    }

}
