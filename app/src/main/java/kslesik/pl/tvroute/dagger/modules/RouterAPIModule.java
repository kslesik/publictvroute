package kslesik.pl.tvroute.dagger.modules;

import android.app.Application;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kslesik.pl.tvroute.R;
import kslesik.pl.tvroute.api.router.RouterAPI;
import kslesik.pl.tvroute.converters.StringToChannelConverter;
import kslesik.pl.tvroute.dagger.qualifiers.RouterApiClient;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by maxym on 06.02.2016.
 */
@Module(includes = HttpClientsModule.class)
public class RouterAPIModule {

    @Provides
    @Singleton
    RouterAPI provideRestAdapter(Application application, @RouterApiClient OkHttpClient okHttpClient, StringToChannelConverter stringToChannelConverter) {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setClient(new OkClient(okHttpClient))
                .setConverter(stringToChannelConverter)
                .setEndpoint(application.getString(R.string.routerApiUrl));
        return builder.build().create(RouterAPI.class);
    }
}
