package kslesik.pl.tvroute.dagger.modules;

import android.app.Application;

import com.squareup.okhttp.OkHttpClient;

import java.util.Map;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kslesik.pl.tvroute.R;
import kslesik.pl.tvroute.api.filmweb.FilmwebAPI;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetAllChannels;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetChannelSchedule;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetProgramDescription;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserFilmWantToSee;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserVotes;
import kslesik.pl.tvroute.converters.StringToJSONArrayConverter;
import kslesik.pl.tvroute.dagger.qualifiers.FilmwebAPIClient;
import kslesik.pl.tvroute.model.Channel;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by maxym on 06.02.2016.
 */
@Module(includes = HttpClientsModule.class)
public class FilmwebAPIModule {

    @Provides
    @Singleton
    FilmwebAPI provideRestAdapter(Application application, @FilmwebAPIClient OkHttpClient okHttpClient) {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setClient(new OkClient(okHttpClient))
                .setConverter(new StringToJSONArrayConverter())
                .setEndpoint(application.getString(R.string.filmwebApiUrl));
        return builder.build().create(FilmwebAPI.class);
    }

    @Provides
    GetChannelSchedule getChannelSchedule() {
        return new GetChannelSchedule();
    }

    @Provides
    GetProgramDescription getProgramDescription() {
        return new GetProgramDescription();
    }

    @Provides
    @Singleton
    GetAllChannels provideGetAllChannels() {
        return new GetAllChannels();
    }

    @Provides
    @Singleton
    GetUserFilmWantToSee provideGetUserFilmWantToSee() {
        return new GetUserFilmWantToSee();
    }

    @Provides
    @Singleton
    GetUserVotes provideGetUserVotes() {
        return new GetUserVotes();
    }

    @Provides
    Map<String, Channel> getAllChannelsList(GetAllChannels getAllChannels){
        return getAllChannels.getResponse();
    }
}
