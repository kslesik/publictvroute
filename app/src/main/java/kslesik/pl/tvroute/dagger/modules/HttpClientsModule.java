package kslesik.pl.tvroute.dagger.modules;

import com.squareup.okhttp.OkHttpClient;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kslesik.pl.tvroute.api.filmweb.Executer;
import kslesik.pl.tvroute.dagger.qualifiers.FilmwebAPIClient;
import kslesik.pl.tvroute.dagger.qualifiers.RouterApiClient;
import kslesik.pl.tvroute.interceptors.UserAgentInterceptor;

/**
 * Created by maxym on 06.02.2016.
 */
@Module
public class HttpClientsModule {

    @Provides
    @Singleton
    @RouterApiClient
    OkHttpClient provideRouterClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
        return okHttpClient;
    }

    @Provides
    @Singleton
    @FilmwebAPIClient
    OkHttpClient provideFilmwebClient(CookieManager cookieManager) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(10, TimeUnit.SECONDS);
        okHttpClient.setCookieHandler(cookieManager);
        okHttpClient.interceptors().add(new UserAgentInterceptor(Executer.USER_AGENT));
        return okHttpClient;
    }

    @Provides
    CookieManager provideSaveCookiesManager(){
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        return cookieManager;
    }
}
