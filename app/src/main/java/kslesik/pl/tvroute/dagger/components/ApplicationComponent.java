package kslesik.pl.tvroute.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import kslesik.pl.tvroute.dagger.modules.FilmwebAPIModule;
import kslesik.pl.tvroute.dagger.modules.MainModule;
import kslesik.pl.tvroute.dagger.modules.RouterAPIModule;
import kslesik.pl.tvroute.view.OverlayFragment;
import kslesik.pl.tvroute.view.ProgramInfoActivity;
import kslesik.pl.tvroute.view.WatchChannelActivity;

@Singleton
@Component(modules = {MainModule.class, FilmwebAPIModule.class, RouterAPIModule.class})
public interface ApplicationComponent {
    void inject(WatchChannelActivity watchChannelActivity);
    void inject(OverlayFragment overlayFragment);
    void inject(ProgramInfoActivity programInfoActivity);
}