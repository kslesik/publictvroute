package kslesik.pl.tvroute.dagger.qualifiers;

import javax.inject.Qualifier;

/**
 * Created by maxym on 06.02.2016.
 */
@Qualifier
public @interface RouterApiClient {
}
