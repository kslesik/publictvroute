package kslesik.pl.tvroute.view;

import android.app.Activity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

import javax.inject.Inject;

import kslesik.pl.tvroute.R;
import kslesik.pl.tvroute.TvRouteApplication;
import kslesik.pl.tvroute.api.filmweb.Executer;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetProgramDescription;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserFilmWantToSee;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserVotes;
import kslesik.pl.tvroute.model.ProgramTV;

/**
 * Created by maxym on 16.01.2016.
 */
@EActivity(R.layout.program_info_layout)
@WindowFeature(Window.FEATURE_NO_TITLE)
@Fullscreen
public class ProgramInfoActivity extends Activity {

    @ViewById
    TextView programTitle;

    @ViewById
    TextView programInfoText;

    @ViewById
    ImageView programImage;

    @ViewById
    LinearLayout wantToSee;

    @ViewById
    TextView filmRate;

    @Extra
    ProgramTV program;

    @Inject
    GetProgramDescription getProgramDescription;

    @Inject
    GetUserFilmWantToSee getUserFilmWantToSee;

    @Inject
    GetUserVotes getUserVotes;

    @Inject
    Executer executer;

    @Inject
    ImageLoader imageLoader;

    @AfterViews
    void init(){

        ((TvRouteApplication) getApplication()).getComponent().inject(this);

        programInfoText.setMovementMethod(new ScrollingMovementMethod());

        if(!program.getProgramImage().equals("null"))
            imageLoader.displayImage(ProgramTV.POSTERS_URL + program.getProgramImage(), programImage);

        programTitle.setText(program.getProgramName());
        programInfoText.setText(program.getProgramInfo()+"\n");
        if(program.getFilmwebProgramID() != null)
            getProgramDescriptionBackgound(program.getFilmwebProgramID());

        if(getUserFilmWantToSee.ifWantToSee(program.getFilmwebProgramID()))
            wantToSee.setVisibility(View.VISIBLE);
        else {
            wantToSee.setBackground(null);
        }

        if(getUserVotes.ifVoted(program.getFilmwebProgramID())){
            wantToSee.setVisibility(View.VISIBLE);
            filmRate.setVisibility(View.VISIBLE);
            filmRate.setText(String.valueOf(getUserVotes.getFilmRating(program.getFilmwebProgramID())));
        }

    }

    @Background
    public void getProgramDescriptionBackgound(long programID) {
        getProgramDescription.setProgramID(programID);
        executer.execute(getProgramDescription);
        setProgramInfo(getProgramDescription.getResponse());
    }

    @UiThread
    void setProgramInfo(String programInfo){
        this.programInfoText.append(programInfo);
    }

}
