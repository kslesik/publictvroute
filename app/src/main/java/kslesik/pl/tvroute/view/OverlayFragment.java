package kslesik.pl.tvroute.view;

import android.app.Fragment;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Handler;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;

import javax.inject.Inject;

import kslesik.pl.tvroute.R;
import kslesik.pl.tvroute.TvRouteApplication;
import kslesik.pl.tvroute.api.filmweb.Executer;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetChannelSchedule;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetProgramDescription;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserFilmWantToSee;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetUserVotes;
import kslesik.pl.tvroute.model.Channel;
import kslesik.pl.tvroute.model.ProgramTV;
import lombok.Setter;

/**
 * Created by maxym on 11.02.2016.
 */
@EFragment(R.layout.overlay_layout_fragment)
public class OverlayFragment extends Fragment {

    public final static String TAG = "overlay_info_fragment";

    private WatchChannelActivity watchChannelActivity;
    private @Setter Channel channel;
    private Pair<ProgramTV, ProgramTV> actuallyPlaying;

    private static final long DELAY_TIME = 3000;
    private static final int PROGRAM_INFO_ACTIVITY = 0;
    private final Handler myHandler = new Handler();
    private Runnable closeControls;

    @App
    TvRouteApplication tvRouteApplication;

    @ViewById
    protected ImageView channelIcon;

    @ViewById
    protected TextView currentDate;

    @ViewById
    protected TextView currentTime;

    @ViewById
    protected TextView firstProgramTime;

    @ViewById
    protected TextView firstProgramName;

    @ViewById
    protected TextView secondProgramTime;

    @ViewById
    protected TextView secondProgramName;

    @ViewById
    protected TextView channelName;

    @ViewById
    protected TextView channelNumber;

    @ViewById
    protected ProgressBar PROGRESS_BAR;

    @ViewById
    LinearLayout wantToSee;

    @ViewById
    TextView filmRate;

    @Inject
    GetChannelSchedule getChannelSchedule;

    @Inject
    GetUserFilmWantToSee getUserFilmWantToSee;

    @Inject
    GetUserVotes getUserVotes;

    @Inject
    Executer executer;

    @Inject
    GetProgramDescription getProgramDescription;

    @Inject
    ImageLoader imageLoader;

    @Click(R.id.firstProgramName)
    public void firstProgramClicked() {
        myHandler.removeCallbacks(closeControls);
        showProgramInfo(actuallyPlaying.first);
    }

    @Click(R.id.secondProgramName)
    public void secondProgramClicked() {
        myHandler.removeCallbacks(closeControls);
        showProgramInfo(actuallyPlaying.second);
    }

    private void showProgramInfo(ProgramTV programTV){
        ProgramInfoActivity_.intent(this).extra("program", programTV).startForResult(PROGRAM_INFO_ACTIVITY);
    }

    @OnActivityResult(PROGRAM_INFO_ACTIVITY)
    public void resumeTimer(){
        myHandler.postDelayed(closeControls, DELAY_TIME);
    }

    @AfterViews
    void init() {
        tvRouteApplication.getComponent().inject(this);
        watchChannelActivity = (WatchChannelActivity)getActivity();

        closeControls = new Runnable() {
            public void run() {
                watchChannelActivity.getFragmentManager().popBackStack();
            }
        };

        final float[] roundedCorners = new float[]{5, 5, 5, 5, 5, 5, 5, 5};
        ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null, null));

        pgDrawable.getPaint().setColor(0xFF0EBFE9);

        ClipDrawable progress = new ClipDrawable(pgDrawable, Gravity.START, ClipDrawable.HORIZONTAL);
        PROGRESS_BAR.setProgressDrawable(progress);

        PROGRESS_BAR.setMax(100);

        loadProgramSchedule();
    }

    @Background
    void loadProgramSchedule() {
        myHandler.removeCallbacks(closeControls);
        getChannelSchedule.setChannel(channel);
        executer.execute(getChannelSchedule);
        setInformations();
    }

    @UiThread
    void setInformations() {

        myHandler.removeCallbacks(closeControls);

        channelNumber.setText(String.valueOf(channel.getChannelID()));

        imageLoader.displayImage(Channel.CHANNELS_ICONS_URL + channel.getChannelIconURL(), channelIcon);
        currentDate.setText(DateTime.now().toString("EEE, d.MM"));
        currentTime.setText(DateTime.now().toString("HH:mm"));
        channelName.setText(channel.getChannelName());
        actuallyPlaying = getChannelSchedule.getActuallyPlaying();
        firstProgramName.setText(actuallyPlaying.first.getProgramName());
        firstProgramTime.setText(actuallyPlaying.first.getTime().toString("HH:mm"));
        secondProgramName.setText(actuallyPlaying.second.getProgramName());
        secondProgramTime.setText(actuallyPlaying.second.getTime().toString("HH:mm"));

        long percent = (DateTime.now().minus(actuallyPlaying.first.getTime().getMillis()).getMillis() * 100 / (actuallyPlaying.second.getTime().minus(actuallyPlaying.first.getTime().getMillis()).getMillis() + 1));

        PROGRESS_BAR.setProgress((int) percent);

        if (getUserFilmWantToSee.ifWantToSee(actuallyPlaying.first.getFilmwebProgramID()))
            wantToSee.setVisibility(View.VISIBLE);

        if (getUserVotes.ifVoted(actuallyPlaying.first.getFilmwebProgramID())) {
            wantToSee.setVisibility(View.VISIBLE);
            wantToSee.setBackground(null);
            filmRate.setVisibility(View.VISIBLE);
            filmRate.setText(String.valueOf(getUserVotes.getFilmRating(actuallyPlaying.first.getFilmwebProgramID())));
        }

        myHandler.postDelayed(closeControls, DELAY_TIME);
    }

    @Override
    public void onDestroy(){
        myHandler.removeCallbacks(closeControls);
        super.onDestroy();
    }


}
