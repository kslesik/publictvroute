package kslesik.pl.tvroute.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.GridView;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;
import org.androidannotations.annotations.res.BooleanRes;
import org.json.JSONException;
import org.videolan.libvlc.LibVlcException;

import java.io.IOException;
import java.io.Serializable;

import javax.inject.Inject;

import kslesik.pl.tvroute.R;
import kslesik.pl.tvroute.TvRouteApplication;
import kslesik.pl.tvroute.api.filmweb.Executer;
import kslesik.pl.tvroute.api.filmweb.calls.get.GetChannelSchedule;
import kslesik.pl.tvroute.api.filmweb.exceptions.UserNotLogInException;
import kslesik.pl.tvroute.player.Player;
import kslesik.pl.tvroute.view.adapters.ImageAdapter;
import retrofit.RetrofitError;

/**
 * Created by maxym on 04.01.2016.
 */
@EActivity(R.layout.watch_channel_layout)
@Fullscreen
@WindowFeature(Window.FEATURE_NO_TITLE)
public class WatchChannelActivity extends Activity implements Serializable {

    @App
    TvRouteApplication tvRouteApplication;

    @Inject
    Executer executer;

    @Inject
    Player player;

    @Inject
    GetChannelSchedule getChannelSchedule;

    @Inject
    OverlayFragment overlayFragment;

    @SystemService
    ConnectivityManager connectivityManager;

    @SystemService
    AudioManager audioManager;

    @ViewById
    SurfaceView channelSurface;

    @ViewById
    GridView channelMenu;

    @ViewById
    FrameLayout  overlayTest;

    @BooleanRes(R.bool.isLand)
    boolean isLandscape;

    @UiThread
    void showCloseAlert(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Zamknij",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alertDialog.show();

    }


    @UiThread
    void getData() {
        Dialog dialog = ProgressDialog.show(this, "Loading", "Please wait...", true);
        getDataBackground(dialog);
    }

    @Background
    public void getDataBackground(Dialog dialog) {
        try {
            executer.getData();
            player.init(channelSurface, this);
            player.startStreaming();
            player.startPlaying();
            showProgramOverlay(true);
        } catch(UserNotLogInException e){
            showCloseAlert(getResources().getString(R.string.login_filmweb_error));
        } catch(RetrofitError e) {
            Log.wtf(this.getClass().getName(), e);
            showCloseAlert("Can't open stream");
        } catch(LibVlcException e){
            showCloseAlert(e.getMessage());
            Log.wtf(this.getClass().getName(), e);
        } finally{
            dialog.dismiss();
        }
    }


    @Override
    public void onDestroy() {
        player.close();
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private boolean isOnline() {
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isAvailable() && netInfo.isConnected();
    }


    private float x1, x2;
    private float MIN_DISTANCE = 200;

    @Touch(R.id.channelSurface)
    public void onTouch(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;

                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    if (x2 < metrics.widthPixels / 3 && x1 < metrics.widthPixels / 3) {
                        if (x2 > x1) {
                            channelMenu.setTranslationX(0);
                            channelMenu.bringToFront();
                            channelMenu.setAdapter(new ImageAdapter(this, player.getChannelsListOnMux()));
                        } else {
                            channelMenu.setTranslationX(-channelMenu.getWidth());
                        }
                    } else {
                        if (x2 > x1) {
                            // Toast.makeText(this, "Previous channel", Toast.LENGTH_LONG).show();
                            playPreviousChannel();
                        } else {
                            //Toast.makeText(this, "Next  channel", Toast.LENGTH_LONG).show();
                            playNextChannel();

                        }
                    }
                } else {
                    showProgramOverlay(false);
                }
                break;
        }
    }

    @ItemClick(R.id.channelMenu)
    public void channelClicked(int position) {
        channelMenu.setTranslationX(-channelMenu.getWidth());
        player.playChannel(position);
        showProgramOverlay(true);
    }

    private void playPreviousChannel() {

        if (!player.previousChannelAvaivaible()) {
            player.stop();
            playPreviousMux();
            return;
        }

        player.playPreviousChannel();
        showProgramOverlay(true);
    }


    private void playNextChannel() {
        if (!player.nextChannelAvaivaible()) {
            player.stop();
            playNextMux();
            return;
        }

        player.playNextChannel();
        showProgramOverlay(true);
    }

    @UiThread
    void playNextMux() {
        Dialog dialog = ProgressDialog.show(this, "Changing mux", "Please wait...", true);
        switchMux(dialog, true);
    }

    @UiThread
    void playPreviousMux() {
        Dialog dialog = ProgressDialog.show(this, "Changing mux", "Please wait...", true);
        switchMux(dialog, false);
    }

    @Background
    void switchMux(Dialog dialog, boolean mode) {
        try {
            player.switchMux(mode);
            showProgramOverlay(true);
        } catch (JSONException | IOException e) {
            Log.wtf(this.getClass().getName(), e);
            showCloseAlert("Can't change mux");
        } finally {
            dialog.dismiss();
        }

    }

    @UiThread
    void showProgramOverlay(boolean forceView) {

        Fragment overlayInfoFragment = getFragmentManager().findFragmentByTag(OverlayFragment.TAG);

        if (overlayInfoFragment != null) {
            if(forceView){
                overlayFragment.setChannel(player.getCurrentlyPlayingChannel());
                overlayFragment.loadProgramSchedule();
            } else getFragmentManager().popBackStack();
        } else {
            showProgramOverlayFragment();
        }
    }

    private void showProgramOverlayFragment(){
        overlayFragment.setChannel(player.getCurrentlyPlayingChannel());
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_in, R.anim.fade_out, R.anim.fade_out)
                .add(R.id.overlayTest, overlayFragment, OverlayFragment.TAG)
                .addToBackStack(null).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isOnline()) {
            showCloseAlert("No active internet connection!");
            return;
        }

        tvRouteApplication.getComponent().inject(this);

        getData();

    }

}
