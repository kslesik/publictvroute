package kslesik.pl.tvroute.view.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import kslesik.pl.tvroute.model.Channel;

public class ImageAdapter extends BaseAdapter {

    private Context context;
    private List<Channel> channelsList;

    private final ImageLoader imageLoader;

    public ImageAdapter(Context context, List<Channel> channelsList){
        this.context = context;
        this.channelsList = channelsList;

        imageLoader = ImageLoader.getInstance();

        if (!imageLoader.isInited())
            imageLoader.init(ImageLoaderConfiguration.createDefault(context));

    }

    @Override
    public int getCount() {
        return channelsList.size();
    }

    @Override
    public Channel getItem(int position) {
        return channelsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return channelsList.get(position).getChannelID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 200));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        imageLoader.displayImage(Channel.CHANNELS_ICONS_URL+channelsList.get(position).getChannelIconURL(), imageView);

        return imageView;
    }
}