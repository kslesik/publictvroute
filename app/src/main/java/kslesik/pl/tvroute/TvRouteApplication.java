package kslesik.pl.tvroute;

import android.app.Application;

import org.androidannotations.annotations.EApplication;

import kslesik.pl.tvroute.dagger.components.ApplicationComponent;
import kslesik.pl.tvroute.dagger.components.DaggerApplicationComponent;
import kslesik.pl.tvroute.dagger.modules.MainModule;

@EApplication
public class TvRouteApplication extends Application {
    private ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerApplicationComponent.builder()
                .mainModule(new MainModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }
}